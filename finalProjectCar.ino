/**********SHAKE RACE REMOTE**********************
*This is the code for the car for Shake Race.    *
*This code reads from the Serial monitor to      *
*determine what the car should do (move forward, *
*move forward while turning left/right, turn the *
*cannon, fire the cannon). The car also senses   *
*being hit by light, which would freeze it for   *
*1.5 seconds                                     *
**************************************************/

#include "Servo.h"

//Pin values
int servoPin1 = 10;
int servoPin2 = 9;
int servoPin3 = 12;
int lightPin = 13;

int lightSensor = 0;
int cannonDirection = 90;

//Variables used for timing
unsigned long startTime = 0;
unsigned long currentTime;

boolean lightIsOn = false; //keeps track of the condiiton of the light

//Servo declarations
Servo leftWheel;
Servo rightWheel;
Servo cannonRotate;

/*Definitions for XBee communication. These are
identical in the code for the remote*/
#define  FORWARD  'A'
#define  RIGHT    'B'
#define  LEFT     'C'
#define  CANNONL  'D'
#define  CANNONR  'E'
#define  CANNONS  'F'
#define  CANNOND  'G'

void setup(){
  pinMode(servoPin1, OUTPUT);
  pinMode(servoPin2, OUTPUT);
  pinMode(servoPin3, OUTPUT);
  pinMode(lightPin, OUTPUT);
  pinMode(lightSensor, INPUT);
  leftWheel.attach(servoPin1);
  rightWheel.attach(servoPin2);
  cannonRotate.attach(servoPin3);
  Serial1.begin(9600);
}

/*Checks for input from the remote over Serial and the light sensors to
  detect a hit. Depending on the command that is received, the car behaves
  accordingly.*/
void loop() {
  //Reads in the character from the Xbee
  char byteIn;
  if (Serial1.available() > 0){
    byteIn = (char)Serial1.read();
    while (Serial1.available() > 0)
      Serial1.read();
  }
  
  /*Measures to see if the light sensors are hit or not, and freezes the
  car if hit*/
  Serial.println(analogRead(lightSensor));
  if (analogRead(lightSensor) > 150) {
    stopMoving();
    delay(1500);
  }
  
  cannonRotate.write(cannonDirection);
  
  /*Turns the light off and snaps it back to center if it had been on for
  more than 1.25 seconds*/
  currentTime = millis();
  if (currentTime - startTime > 1250 && lightIsON) {
    digitalWrite(lightPin, LOW);
    lightIsOn = false;
    cannonDirection = 90;
  }
  
  /*depending on what character was read, the car behaves in a particular
  way*/
  switch (byteIn) {
    case FORWARD:
      moveForward();
      break;
    case RIGHT:
      turnRight();
      break;
    case LEFT:
      turnLeft();
      break;
    case CANNONL:
      if (cannonDirection <= 180)
        cannonDirection++;
      break;
    case CANNONR:
      if (cannonDirection >= 0)
        cannonDirection--;
      break;
    case CANNONS:  //shoots the cannon by turning on the light
      digitalWrite(lightPin, HIGH);
      lightIsOn = true;
      startTime = millis();
      break;
    //snaps the cannon back to center when the user has less than 50 shakes
    case CANNOND:
      if (!lightIsOn)
        cannonDirection = 90;
      break;
    default:
      stopMoving();
      break;
  }
  delay(25);
}

/*These functions contain calibrated values to
control the servos on the car for it to move
forward, turn right, turn left, and stop moving*/
void moveForward() {
  leftWheel.write(98);
  rightWheel.write(0);
}

void turnRight() {
  leftWheel.write(180);
  rightWheel.write(88);
}

void turnLeft() {
  leftWheel.write(92);
  rightWheel.write(0);
}

void stopMoving() {
  leftWheel.write(90);
  rightWheel.write(90);
}
