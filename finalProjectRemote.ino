/**********SHAKE RACE REMOTE**********************
*This is the code for the remote for Shake Race. *
*The ultimate point of this code is to send over *
*serial communication with Xbee's commands to a  *
*car, allowing a user to control its motions by  *
*tilting the remote and to aim a light cannon    *
*and perform attacks. The remote also keeps track*
*of the shaking and power bank interface that    *
*makes Shake Race. This code measures and stores *
*shakes locally, as well as depleting shakes     *
*as the car is moving (by the up button being    *
*pressed).                                       *
**************************************************/

#include <LiquidCrystal.h>

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

//Accelerometer pins and info
const int zpin = A1;
const int ypin = A2;
const int xpin = A3;
const int groundpin = A4;
const int powerpin = A5;
const int threshold = 150;

//Variables for LCD keypad shield
int lcd_key = 0;
int adc_key_in = 0;
int numShakes = -1;

//Variables used for timing
unsigned long startTime = 0;
unsigned long currentTime;

//Boolean conditions that are toggled
boolean onWayUp;
boolean isCalibrated = false;
boolean hasStarted = false;
boolean startDepletionCycle = false;

//Accelerometer values
float xval;
float yval;
float zval;
float xbar;
float ybar;
float zbar;
float totMag;

//Definitions for the buttons on keypad shield
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5

/*Definitions for XBee communication. These are
identical in the code for the car*/
#define  FORWARD  'A'
#define  RIGHT    'B'
#define  LEFT     'C'
#define  CANNONL  'D'
#define  CANNONR  'E'
#define  CANNONS  'F'
#define  CANNOND  'G'

void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);
  lcd.begin(16, 2);  
  printInstructions();
  pinMode(zpin, INPUT);
  pinMode(ypin, INPUT);
  pinMode(xpin, INPUT);
  pinMode(groundpin, OUTPUT);
  pinMode(powerpin, OUTPUT);
  Serial.begin(9600); 
  digitalWrite(groundpin, LOW);
  digitalWrite(powerpin, HIGH);
}
 
void loop() {
   //Condition so callibration only happens once
   if (isCalibrated == false) {
     posCal();
     isCalibrated = true;
   }
   
   //numShakes = 95;  //cheat code for testing
   updateAccelVals();
   currentTime = millis();
   lcd_key = read_LCD_buttons();  // read the buttons
   Serial.println(numShakes);
   
   /*depending on which button was pushed, we send something to
   the car or perform an action (like charging the power bank)*/
   switch (lcd_key) {
     case btnRIGHT: //turn cannon right
       Serial1.print(CANNONR);
       break;
     case btnLEFT:  //turn cannon left
       Serial1.print(CANNONL);
       break;
     //move or move and turn depending on how the remote is tilted
     case btnUP:    
       if (numShakes >= 0) {  
         if (xval < 440)
            Serial1.print(LEFT);
         else if (xval > 580)
            Serial1.print(RIGHT);
         else
            Serial1.print(FORWARD);
         //Conditions so shakes are depleted periodically (2/second or 1/click)
         if (startDepletionCycle == false) {
           startDepletionCycle = true;
           numShakes--; //put here to prevent users from spamming up without penalty
           startTime = millis();
         }
         if (currentTime - startTime > 1000 && numShakes >= 0) {
           numShakes--;
           startDepletionCycle = false;
         }
         printPowerBar();
         break;
       case btnDOWN: //fire if user has more than 50 shakes or snap to center
         if (numShakes >= 50 && numShakes < 100) {
           numShakes = numShakes - 50;
           Serial1.print(CANNONS);
         }
         /*prevents users with 100 shakes trying to perform an attack from losing
         100 as it quickly loops through twice*/
         if (numShakes == 100) {
           numShakes = 49;
           Serial1.print(CANNONS);
         }
         else
           Serial1.print(CANNOND);
         break;
       case btnSELECT: //starts detecting shakes
         hasStarted = true;
         shakeDetect();
         printPowerBar();
         break;
       case btnNONE: //prints the power bar when nothing is pressed
         if (hasStarted)
           printPowerBar();
         if (startDepletionCycle)
           startDepletionCycle = false;
         delay(5);
         break;
       }
   }
}

/*This function calibrates the starting orientation and sets xbar, 
ybar, and zbar to values that represent the Arduino when it is not 
being shaked */
void posCal(){
  float xtot = 0;
  float ytot = 0;
  float ztot = 0;
  for (int i = 0; i < 100; i++){
    xval = float(analogRead(xpin));
    yval = float(analogRead(ypin));
    zval = float(analogRead(zpin));
    xtot = xtot + xval;
    ytot = ytot + yval;
    ztot = ztot + zval;
    delay(1);
  }
  xbar = xtot/100;
  ybar = ytot/100;
  zbar = ytot/100;
  Serial.print(xbar);
  Serial.write('\t');
  Serial.print(ybar);
  Serial.write('\t');
  Serial.print(zbar);
  Serial.write('\n');
}

//This function reads the buttons
int read_LCD_buttons() {
  adc_key_in = analogRead(0);
  if (adc_key_in < 50)   return btnRIGHT;  
  if (adc_key_in < 195)  return btnUP; 
  if (adc_key_in < 380)  return btnDOWN; 
  if (adc_key_in < 555)  return btnLEFT; 
  if (adc_key_in < 790)  return btnSELECT;   
  return btnNONE;  // when all others fail, return this...
}

/*This function uses the running total magnitude of acceleration value to
determine if there was a shake and increments the number of shakes*/
void shakeDetect(){
  xval = float(analogRead(xpin));    
  yval = float(analogRead(ypin));
  zval = float(analogRead(zpin));
  //xval, yval, and zval are used to measure totMag each sample
  totMag = sqrt((xval-xbar)*(xval-xbar) + (yval-ybar)*(yval-ybar) + (zval-zbar)*(zval-zbar));
  if (totMag > threshold && onWayUp == false){
     onWayUp = true;
     if (numShakes < 100)    
       numShakes++;    //increments the step counter each time the magnitude goes above the threshold
  }
  if (totMag < threshold && onWayUp == true)
     onWayUp = false;  //notes that a single step is no longer being measured
}

/*This function prints the instructions that play through when 
Arduino first turns on. They can be skipped by pressing and holding
select*/
void printInstructions() {
  String instructions[] = {"--Instructions--",
                           "----------------",
                           "Press up to move",
                           "your car forward",
                           "----------------",
                           "Press right and ",
                           "left to turn the",
                           "cannon          ",
                           "----------------",
                           "Press down to   ",
                           "fire your cannon",
                           "----------------",
                           "Tilt while pres-",
                           "sing up to turn ",
                           "the car         ",
                           "----------------",
                           "Moving and perf-",
                           "orming an attack",
                           "will drain your ",
                           "power bank      ",
                           "----------------",
                           "To charge up,   ",
                           "press select and",
                           "shake your cont-",
                           "roller          ",
                           "----------------",
                           "Hold select to  ",
                           "charge your car!"};
  for (int i = 0; i < 27; i++){
    //see if select is being pressed
    if (read_LCD_buttons() == btnSELECT)
      i = 26;
    lcd.setCursor(0,0);
    lcd.print(instructions[i]);
    lcd.setCursor(0,1);
    lcd.print(instructions[i+1]);
    delay(1000);
  }
}

//This function prints the power bar to the LCD
void printPowerBar(){
  lcd.setCursor(0,0);
  lcd.print("POWER:");
  lcd.setCursor(0,1);
  lcd.print("BANK :");
  int topBarSlots = numShakes%10;
  int bottomBarSlots = numShakes/10;
  lcd.setCursor(6,0);
  for (int i = 0; i < 10; i++)
    if (i < topBarSlots)
      lcd.print((char)219);
    else
      lcd.print(" ");
  lcd.setCursor(6,1);
  for (int j = 0; j < 10; j++)
    if (j < bottomBarSlots)
      lcd.print((char)219);
    else
      lcd.print(" ");
  lcd.setCursor(0,0);
}

//This function updates the acceleration values
void updateAccelVals(){
  xval = float(analogRead(xpin));    
  yval = float(analogRead(ypin));
  zval = float(analogRead(zpin));
}
