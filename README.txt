We built a set of racing cars and remotes that are controlled with a pair of
Arduinos that communicate with XBee's. The Arduino on the remote is the
transmitting XBee and the Arduino on the car is the receiving one. These cars
are given a virtual power bank that can be charged by shaking the remote. This
can only be done when the car is not moving. This action will be measured by the
accelerometers on the remote. Each controller will have an LCD display that
shows how much power the user has saved up. Moving the car by pressing up on the
keypad will quickly drain the power bank (2 shakes / second). The power bank can
also be drained by performing an attack from an aimable light cannon that is
mounted to the top of each car. By pressing right and left on the keypad, the
servo that the flashlight is mounted to changes orientation. The attack is fired
by pressing down on the keypad, using up 50 shakes. This turns on the flashlight
using a relay that is connected to a digital pin on the Arduino. If the attack
that is performed hits the back of the other person's car (sensed by three
photoresistors in series that are pointed as can be seen in the attached
picture) their opponent will be disabled from controlling it for 1.5 seconds,
giving them time to catch up as their opponent is stopped. During the game,
players will have to decide between how much time they spend stopped and shaking
and how much time they spend racing and trying to attack their opponent, knowing
that it has a cost. Looking forward, this project can be transformed into a game
that is somewhat of a timed battle arena, where a hit counts as a point and
being hit takes away a point. For this to be possible, the game would have to be
transferred to Wifi communication, so that it can be networked and (timed). The
usage of Wifi will allow for the cars to communicate back to the remotes that
they were hit, and for the remotes to communicate with each other score, timing,
and other information.

For the project we used 2x of (the two sets of cars/remotes are identical): 2
Arduinos, 1 Set of XBee's, 1 Flashlight, 1 5V Relay, 2 Continuous Rotation
Servos, 1 Standard (180 degree) Servo, 1 Boebot frame, 1 Accelerometer, 3
Photoresistors, 1 LCD Keypad Shield, 2 Extender Shields
